#include <limits.h>
#include <wait.h>
#include <pwd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h> //library thread
#include "snippedBase64.h"

void waitChildToDie(){
  int status;
  while(wait(&status) > 0);
}

void combineTwoString(char dst[], char str1[], char str2[]){
  strcpy(dst, str1);
  strcat(dst, str2);
}

void reverseString(char dst[], char src[]){
  int len_src = strlen(src);
  int id = 0;
  for(int i = len_src-1;i>=0;i--){
    dst[id] = src[i];
    id+=1; 
  }
  dst[id] = 0;
}

void checkDirectory(){
   char cwd[PATH_MAX];
   if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("The working is this dir sir: %s\n", cwd);
   } else {
       perror("getcwd() error");
   }  
}

void downloadFile(char url[], char location[]){
  pid_t childAbuse_download = fork();
  if(childAbuse_download < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_download == 0){
    //chid
    execlp("wget", "wget", "--no-check-certificate", url, "-O", location, "-q", NULL);
  }
  else{
    waitChildToDie();
  }
}

void createDir(char directoryName[]){
  // mengefork to create dir
  pid_t childAbuse_mkdirId = fork();
  if(childAbuse_mkdirId < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_mkdirId == 0){
    // li shi bru, we are in child
    execlp("mkdir", "mkdir","-p",directoryName, NULL);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void unzipit(char fileLocation[], char savedLoc[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-qq",fileLocation, "-d", savedLoc,NULL);
  }
  else{
    waitChildToDie();
  }
}

void deleteDir(char dirRelativePath[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("rm", "rm",  "-rf",dirRelativePath,NULL);
    }
    else{
        waitChildToDie();
    }
}

void copyFile(char filePath[], char destination[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("cp", "cp",filePath, destination,NULL);
    }
    else{
        waitChildToDie();
    }
}

void deleteFile(char filePath[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("rm", "rm",filePath,NULL);
    }
    else{
        waitChildToDie();
    }
}

void moveFile(char filePath[], char destination[]){
    pid_t childAbuse_mv = fork();
    if(childAbuse_mv < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_mv == 0){
        execlp("mv", "mv",filePath, destination,NULL);
    }
    else{
        waitChildToDie();
    }
}

void passwordZipFile(char dirPath[], char addition[], char destination[], char password[]){
    pid_t childAbuse_zip = fork();
    if(childAbuse_zip < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_zip == 0){
        printf("%s | %s\n", destination, dirPath);
        if(addition != NULL){
            execlp("zip", "zip","-rP", password, destination, dirPath, addition, NULL);
        }else{
            execlp("zip", "zip","-rP", password, destination, dirPath, NULL);
        }
        
    }
    else{
        waitChildToDie();
    }
}

void passwordUnZipFile(char fileLocation[],char savedLoc[],char password[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-oP", password, fileLocation, "-d", savedLoc,NULL);
  }
  else{
    waitChildToDie();
  }

}

void writeTextFile(char textFilePath[], char input[]){
    FILE *fptr;
    int len = strlen(input);
    fptr = fopen(textFilePath,"a");//append sapa tau mau nambah
    if(input[len-1]=='\n'){
        fprintf(fptr,"%s",input);
    }else{
        fprintf(fptr,"%s\n",input);
    }
    
    fclose(fptr);
}

void traverseDir(char thepath[PATH_MAX]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
        printf("%s\n", ep->d_name);
    }

    (void) closedir (dp);
  } else perror ("Couldn't open the directory");

}

void readTextFile(char textFilePath[], char *output, unsigned maxlen){
    FILE *fp = fopen(textFilePath, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", textFilePath);
        return;
    }

    // reading line by line, max 256 bytes
    fgets(output, maxlen, fp);
    // printf("%s", buffer);
    // free(buffer);
    // close the file
    fclose(fp);
}

// Global variable to save thread id
pthread_t tid[3];

void* thread_unzip(void *arg){
    char *zipLoc = malloc(sizeof(char)*128);
    char *unzipLoc = malloc(sizeof(char)*128);
    char *name = (char *) arg;
    // Create ./{name} and ./{name}.zip
    combineTwoString(unzipLoc, "./", name);
    combineTwoString(zipLoc, unzipLoc, ".zip");
    printf("unzip: %s, store in: %s\n", zipLoc, unzipLoc);
    unzipit(zipLoc, unzipLoc);
	free(zipLoc);free(unzipLoc);
	return NULL;
}

void *decodeAllTxtInDir(void *arg){
    char *thepath = malloc(sizeof(char)*256);
    // Make ./{name} string
    char *name = (char *) arg;
    combineTwoString(thepath, "./", name);
    strcat(thepath, "/");
    printf("traversing and decoding txt in: %s\n", thepath);
    DIR *dp;
    struct dirent *ep;
    char path[100];

    dp = opendir(thepath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                //is not another dir, most likely txt
                // printf("%s\n", ep->d_name);
                // Open the file
                char *encodedStr = malloc(sizeof(char)*256);
                char *fileLoc = malloc(sizeof(char)*256);
                combineTwoString(fileLoc, thepath, ep->d_name);
                readTextFile(fileLoc, encodedStr, 256);

                // printf("got: %s\n", encodedStr);
                // Decode it
                char *decodedStr = malloc(sizeof(char)*256);
                size_t size = 256;
                decodedStr = base64_decode(encodedStr, strlen(encodedStr), &size);
                // printf("translated: %s\n", decodedStr);

                // Write to {name}.txt
                //Create the location of txt file
                char *outputLoc = malloc(sizeof(char)*256);
                combineTwoString(outputLoc, thepath, name);
                strcat(outputLoc, ".txt");
                //Write it sir
                writeTextFile(outputLoc, decodedStr);

                free(fileLoc);free(encodedStr);free(decodedStr);
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    printf("Finish decoding\n");
    free(thepath);
    return NULL;
}

void extractUser(char homePath[], char user[]){
    int len = strlen(homePath);
    char temp[256];
    int id = 0;
    for(int i = len-1;i>=0;i--){
        if(homePath[i]=='/'){
            break;
        }
        temp[id] = homePath[i];
        id+=1;
    }
    temp[id] = 0;
    char reversed[256];
    reverseString(reversed, temp);
    strcpy(user, reversed);
}

void *createTextFileNo(void *arg){
    writeTextFile("./no.txt", "No");

}

void *passwordUnzipThread(void *arg){
    char *password = (char *) arg;
    passwordUnZipFile("./hasil.zip", "./", password);
}

int main(){
    //get user homedir of whoever running this program
    char *homedir;
    // check ada variable home kah di enviromentnya
    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    char root[100] = "";
    combineTwoString(root, homedir, "/shift3/"); //this is where we do our work
    printf("%s\n", root);
    if ((chdir(root)) < 0) {
        printf("FAIL\n");
        exit(EXIT_FAILURE);
    }
    
    // Download music and quote.zip
    char quote_link[] = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";
    char music_link[] = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
    downloadFile(quote_link, "./quote.zip");
    downloadFile(music_link, "./music.zip");

    // int pthread_create(pthread_t *restrict tidp,
    //                const pthread_attr_t *restrict attr,
    //                void *(*start_rtn)(void *),
    //                void *restrict arg);
    
    // Unzip the zip using thread to make it faster
    int err;
    char *names[2] = {"quote", "music"};
    for(short i = 0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&thread_unzip,names[i]); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

    // Decode all the txt
    for(short i = 0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&decodeAllTxtInDir,names[i]); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

    // Create folder hasil
    createDir("hasil");
    // Move it to hasil
    moveFile("./music/music.txt", "./hasil/music.txt");
    moveFile("./quote/quote.txt", "./hasil/quote.txt");

    // Zip it
    char *user = malloc(sizeof(char)*256);
    extractUser(homedir, user);
    char *password = malloc(sizeof(char)*256);
    combineTwoString(password, "mihinomenest", user);
    passwordZipFile("./hasil/", NULL,"./hasil.zip", password);
    
    //Lupa ceritanya, jadi di unzip lagi
    err=pthread_create(&(tid[0]),NULL,&passwordUnzipThread, password);
    if(err!=0) //cek error
    {
        printf("\n can't create thread : [%s]",strerror(err));
    }
    err=pthread_create(&(tid[1]),NULL,&createTextFileNo,NULL);

    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

    passwordZipFile("./hasil/", "no.txt","./hasil.zip", password);
    free(user);free(password);
    return 0;
}