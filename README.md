# Soal Shift Sisop Modul 3 E01 2022

Repo untuk soal shift sisop modul 3


## DAFTAR ISI ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomor 1](#nomor-1)
    - [Soal  1.a](#soal-1a)
    - [Soal  1.b](#soal-1b)
    - [Soal  1.c](#soal-1c)
    - [Soal  1.d](#soal-1d)
    - [Soal  1.e](#soal-1e)
- [Nomor 2](#nomor-2)
    - [Soal  2.a](#soal-2a)
    - [Soal  2.b](#soal-2b)
    - [Soal  2.c](#soal-2c)
    - [Soal  2.d](#soal-2d)
    - [Soal  2.e](#soal-2e)
    - [Soal  2.f](#soal-2f)
    - [Soal  2.g](#soal-2g)
- [Nomor 3](#nomor-3)
    - [Soal  3.a](#soal-3a)
    - [Soal  3.b](#soal-3b)
    - [Soal  3.c](#soal-3c)
    - [Soal  3.d](#soal-3d)
    - [Soal  3.e](#soal-3e)
- [Dokumentasi](#dokumentasi)
## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201237  | Putu Ravindra Wiguna | SISOP E
5025201003    | Rahmat Faris Akbar | SISOP E
05111740000146    | Muhammad Kiantaqwa Farhan | SISOP E

## Nomor 1 ##
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
Catatan :
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2


### Soal 1.a ###
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

#### Dokumentasi Source Code ####

```bash
void downloadFile(char url[], char location[]){
  pid_t childAbuse_download = fork();
  if(childAbuse_download < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_download == 0){
    //chid
    execlp("wget", "wget", "--no-check-certificate", url, "-O", location, "-q", NULL);
  }
  else{
    waitChildToDie();
  }
}

void unzipit(char fileLocation[], char savedLoc[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-qq",fileLocation, "-d", savedLoc,NULL);
  }
  else{
    waitChildToDie();
  }
}
--------------------------------- snipped from main--------------------
    // Download music and quote.zip
    char quote_link[] = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";
    char music_link[] = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
    downloadFile(quote_link, "./quote.zip");
    downloadFile(music_link, "./music.zip");

    // int pthread_create(pthread_t *restrict tidp,
    //                const pthread_attr_t *restrict attr,
    //                void *(*start_rtn)(void *),
    //                void *restrict arg);
    
    // Unzip the zip using thread to make it faster
    int err;
    char *names[2] = {"quote", "music"};
    for(short i = 0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&thread_unzip,names[i]); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```


### Soal 1.b ###
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

#### Dokumentasi Source Code ####

```bash

base64 decoder is from : // https://stackoverflow.com/questions/180947/base64-decode-snippet-in-c/13935718
------------------------------------------------
void *decodeAllTxtInDir(void *arg){
    char *thepath = malloc(sizeof(char)*256);
    // Make ./{name} string
    char *name = (char *) arg;
    combineTwoString(thepath, "./", name);
    strcat(thepath, "/");
    printf("traversing and decoding txt in: %s\n", thepath);
    DIR *dp;
    struct dirent *ep;
    char path[100];

    dp = opendir(thepath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                //is not another dir, most likely txt
                // printf("%s\n", ep->d_name);
                // Open the file
                char *encodedStr = malloc(sizeof(char)*256);
                char *fileLoc = malloc(sizeof(char)*256);
                combineTwoString(fileLoc, thepath, ep->d_name);
                readTextFile(fileLoc, encodedStr, 256);

                // printf("got: %s\n", encodedStr);
                // Decode it
                char *decodedStr = malloc(sizeof(char)*256);
                size_t size = 256;
                decodedStr = base64_decode(encodedStr, strlen(encodedStr), &size);
                // printf("translated: %s\n", decodedStr);

                // Write to {name}.txt
                //Create the location of txt file
                char *outputLoc = malloc(sizeof(char)*256);
                combineTwoString(outputLoc, thepath, name);
                strcat(outputLoc, ".txt");
                //Write it sir
                writeTextFile(outputLoc, decodedStr);

                free(fileLoc);free(encodedStr);free(decodedStr);
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    printf("Finish decoding\n");
    free(thepath);
    return NULL;
}

-------------snipped from main------------
// Decode all the txt
    for(short i = 0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&decodeAllTxtInDir,names[i]); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

```


### Soal 1.c ###
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

#### Dokumentasi Source Code ####

```bash
void moveFile(char filePath[], char destination[]){
    pid_t childAbuse_mv = fork();
    if(childAbuse_mv < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_mv == 0){
        execlp("mv", "mv",filePath, destination,NULL);
    }
    else{
        waitChildToDie();
    }
}
void createDir(char directoryName[]){
  // mengefork to create dir
  pid_t childAbuse_mkdirId = fork();
  if(childAbuse_mkdirId < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_mkdirId == 0){
    // li shi bru, we are in child
    execlp("mkdir", "mkdir","-p",directoryName, NULL);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}
--------------------snipped from main------
// Create folder hasil
createDir("hasil");
// Move it to hasil
moveFile("./music/music.txt", "./hasil/music.txt");
moveFile("./quote/quote.txt", "./hasil/quote.txt");
```


### Soal 1.d ###
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

#### Dokumentasi Source Code ####

```bash
void passwordZipFile(char dirPath[], char addition[], char destination[], char password[]){
    pid_t childAbuse_zip = fork();
    if(childAbuse_zip < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_zip == 0){
        printf("%s | %s\n", destination, dirPath);
        if(addition != NULL){
            execlp("zip", "zip","-rP", password, destination, dirPath, addition, NULL);
        }else{
            execlp("zip", "zip","-rP", password, destination, dirPath, NULL);
        }
        
    }
    else{
        waitChildToDie();
    }
}
------------------------- snipped from main------------
// Zip it
    char *user = malloc(sizeof(char)*256);
    extractUser(homedir, user);
    char *password = malloc(sizeof(char)*256);
    combineTwoString(password, "mihinomenest", user);
    passwordZipFile("./hasil/", NULL,"./hasil.zip", password);
```


### Soal 1.e ###
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

#### Dokumentasi Source Code ####

```bash
void passwordUnZipFile(char fileLocation[],char savedLoc[],char password[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-oP", password, fileLocation, "-d", savedLoc,NULL);
  }
  else{
    waitChildToDie();
  }

}

void *passwordUnzipThread(void *arg){
    char *password = (char *) arg;
    passwordUnZipFile("./hasil.zip", "./", password);
}


 //Lupa ceritanya, jadi di unzip lagi
    err=pthread_create(&(tid[0]),NULL,&passwordUnzipThread, password);
    if(err!=0) //cek error
    {
        printf("\n can't create thread : [%s]",strerror(err));
    }
    err=pthread_create(&(tid[1]),NULL,&createTextFileNo,NULL);

    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

    passwordZipFile("./hasil/", "no.txt","./hasil.zip", password);
    free(user);free(password);
```


## Nomor 2 ##
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria.

Note : 
- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.


### Soal 2.a ###
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:
users.txt
> username:password
> username2:password2



#### Dokumentasi Source Code ####

```bash

```


### Soal 2.b ###
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

#### Dokumentasi Source Code ####

```bash

```


### Soal 2.c ###
Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

#### Dokumentasi Source Code ####

```bash

```


### Soal 2.d ###
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:
> <judul-problem-1> by <author1>
> <judul-problem-2> by <author2>


#### Dokumentasi Source Code ####

```bash

```


### Soal 2.e ###
Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

#### Dokumentasi Source Code ####

```bash

```


### Soal 2.f ###
Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

#### Dokumentasi Source Code ####

```bash

```

### Soal 2.g ###
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

#### Dokumentasi Source Code ####

```bash

```



## Nomor 3 ##
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya. Karena Nami tidak bisa membuat programnya, maka Nami meminta bantuanmu untuk membuat programnya. Bantulah Nami agar programnya dapat berjalan!
Catatan:
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan  fork, exec dan system(),  kecuali untuk bagian zip pada soal d

### Soal 3.a ###
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif


Setelah mengekstrak zip ke direktori `/home/[user]/shift3/` direktori kerja diubah ke `shift3/hartakarun/` terlebih dahulu. Kita butuh kategorisasi untuk bekerja secara rekursif sehingga bisa mengecek file di dalam direktori. Implementasi kodenya adalah sebagai berikut:
```bash
if (entry->d_type == DT_DIR) {
    args *arg = malloc(sizeof(args));
    if (strcmp(entry->d_name, ".") == 0 || strcmp  (entry->d_name, "..") == 0)
        continue;
    snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
    categorize(path, indent + 2);
```

Karena kita butuh untuk memindahkan file ke kategori yang benar, maka kita butuh extension untuk setiap file menggunakan `strchr`.
```bash
char *ext = strchr(filename, '.');
if(!ext || ext == filename)
    return NULL;
return ext + 1;
```

Dan karena direktori bukan case sensitive, maka kita gunakan tolower sehingga extension dapat diconvert ke lower case. Lalu, kita pindahkan file menurut kategori menggunakan `rename`.
```bash
char lowerext[50];
strcpy(lowerext,getext(src));
toLower(lowerext);
mkdir(lowerext,0777);
sprintf(temp,"%s/%s",lowerext,src);
rename(path,temp);
printf("%s move to %s\n", path, temp);
```


### Soal 3.b ###
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.


Untuk melakukan kategorisasi sejumlah file, kita harus mengetahui karakteristik dari setiap file. Dimana unknown files tidak memiliki extension dan hidden file memiliki sebuah titik diawalnya. Untuk mengecek apakah dia unknown file atau bukan dapat menggunakan `getext` sama seperti sebelumnya dan jika dia return null, maka kita buat direktori untuk Unknown file. Lalu, kita pindahkan menggunakan `rename`.
```bash
else if(!getext(src)){
    mkdir("Unknown",0777);
    sprintf(temp,"Unknown/%s",src);
    rename(path,temp);
    printf("%s move to %s\n", path, temp);
}
```

Untuk mengecek hidden file kita harus mengecek apakah `string[0]` adalah titik atau bukan. Kemudian, kita pindahkan dia menggunakan `rename`.
```bash
if(src[0] == '.'){
    mkdir("Hidden",0777);
    sprintf(temp,"Hidden/%s",src);
    rename(path,temp);
    printf("%s move to %s\n", path, temp);
}
```


### Soal 3.c ###
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.


Untuk memindahkan file menggunakan thread kita gunakan `pthread_create` dan menggunakan fungsi move untuk memindahkan file menurut kategorinya. Selain itu, kita gunakan `struct arg` untuk mendapatkan alamat dari setiap file.
```bash
strcpy(arg->arg1,path);
strcpy(arg->arg2,entry->d_name);
err = pthread_create(&tid[pthreadcount++], NULL, &move, (void *)arg);
```

Dan kemudian kita joinkan menggunakan thread
```bash
for(int i = 0; i < pthreadcount; i++){
    pthread_join(tid[i], NULL);
}
```


### Soal 3.d ###
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.


Untuk melakukan zip pada file dapat digunakan `system` dari `shift3/hartakarun ke server
```bash
system("zip hartakarun.zip -r /home/user/shift3/hartakarun");
```


### Soal 3.e ###
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server


Kita dapat menggunakan `argv[]` untuk command dan kemudian kita cek apakah command benar atau tidak.
```bash
//if number of argument is not correct
if(argc != 3){
    perror("Invalid number of arguments\n");
    return -1;
}
//if command is not "send"
if(strcmp(argv[1],"send") != 0){
    perror("Invalid command\n");
    return -1;
}
//if file doesn't exist
if(access(argv[2], F_OK) == -1){
    perror("File not found\n");
    return -1;
}
```
```bash
./client send hartakarun.zip
```

Client Socket:
```bash
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("\n Socket creation error \n");
    return -1;
}
printf("socket created\n");
```

Client Address:
```bash
memset(&serv_addr, '0', sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);
if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
    perror("\nInvalid address/ Address not supported \n");
    return -1;
}
printf("address created\n");
```

Client Connect:
```bash
int con = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
if(con < 0){
    perror("Connection failed\n");
    return -1;
}
printf("Connected to the server\n");
```

Client Socket Server:
```bash
if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("socket failed");
    exit(EXIT_FAILURE);
    }
printf("Socket successfully created\n");
```

Set Socket Server:
```bash
if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
    perror("setsockopt");
    exit(EXIT_FAILURE);
}
```

Create Address dan Bind Server:
```bash
address.sin_family = AF_INET;
address.sin_addr.s_addr = INADDR_ANY;
address.sin_port = htons( PORT );
if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
    perror("bind failed");
    exit(EXIT_FAILURE);
}
printf("Socket successfully binded\n");
```

Server Listening:
```bash
if (listen(server_fd, 3) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
}
```

Server Accepting:
```bash
if ((new_socket = accept(server_fd, (struct sockaddr *)NULL, NULL))<0) {
    perror("accept");
    exit(EXIT_FAILURE);
}
printf("Socket successfully accepted\n");
```

Untuk mengirimkan file ke server, kita harus melakukan read terhadap setiap data dari file dan menetapkan buffernya. Kemudian kita gunakan `send` untuk mengirim buffer ke socket.
```bash
FILE *fp = fopen(filename, "r");
if(fp == NULL){
    perror("Error in reading file\n");
    return -1;
}

send(sock, filename, strlen(filename), 0);
while((valread = fread(buffer, 1, sizeof(buffer), fp))> 0){
    if(send(sock, buffer, valread, 0) < 0){
        perror("Error in sending file\n");
        return -1;
    }
    bzero(buffer, sizeof(buffer));
}
```

Pada server, karena kita tidak memiliki file `hartakarun.zip`, maka kita harus melakukan write terhadap file baru dimana kita dapatkan filename dari client. Kemudian, kita gunakan `recv` untuk menerima data dari client dan melakukan write ke file.
```bash
FILE *fp = fopen(buffer, "wb");
while((b = recv(new_socket, buffer, 1024, 0)) > 0){
    if(fwrite(buffer, 1, b, fp) < 0){
        printf("Error writing to file\n");
        return -1;
        }
    bzero(buffer, 1024);
}
```


## Dokumentasi ##
