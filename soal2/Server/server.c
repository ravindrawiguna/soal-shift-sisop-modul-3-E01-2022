#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

void get_client_msg(int new_socket, char *buffer){
    buffer = malloc(sizeof(char)*1024);
    int valread = read( new_socket , buffer, 1024);
    printf("%s\n",buffer );
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
    printf("creating socket\n");
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    else{
        printf("socket created\n");
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    else{
        printf("setsockopt success\n");
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    // address.
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    else{
        printf("binding success\n");
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    else{
        printf("listen aint error?\n");
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    else{
        printf("accept aint error?\n");
    }

    printf("finish all above\n");
    get_client_msg(new_socket, buffer);
    send(new_socket , hello , strlen(hello) , 0 );
    printf("Hello message sent\n");
    get_client_msg(new_socket, buffer);
    printf("end of server\n");
    return 0;
}